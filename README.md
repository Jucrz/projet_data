# World Population Data Story

## Table des matières

- [Description](#description)
  * [Techonologie utilisées](#techonologie-utilis-es)
- [Installer le projet](#installer-le-projet)
  * [Localement](#localement)
  * [Serveur](#serveur)
- [Traitement / Manipulation des données](#traitement---manipulation-des-donn-es)
- [Inteface](#inteface)
- [Virtualisation](#virtualisation)
- [Hebergement](#hebergement)
- [Souce des données](#souce-des-donn-es)

## Description

Ce projet vise a créer une data story sur l'évolution de la population dans le monde.<br>
Vous pouvez accéder à la data story via ce lien : http://129.151.227.16/

### Techonologie utilisées

**Traitement / Manipulation des données**: Jupyter Notebook<br>
**Interface**: Streamlit<br>
**Virtualisation**: Docker<br>
**Hébergement**: Oracle cloud<br>
**Source des données**: Multiple site (Kaggle, ONU...)

## Installer le projet

Cette partie vise à expliquer comment installer le projet localement ou sur un serveur.

### Localement

Premièrment si vous souhaitez installer le projet localement, vous allez avoir besoin d'installer [docker](https://docs.docker.com/get-started/) sur votre machine.<br>
Vous pouvez aussi installer Make afin de gagner du temps, mais ce n'est pas obligatoire.<br>

Une fois les prérequis installés, vous devez cloner le projet :
````bash
:~$ git clone git@gitlab.com:Fanswers/b3-python-project.git
````
<br>
Desormais placez vous à la racine du projet et effectuez un make build, cela va build une image docker du projet.<br>
Si vous n'avez pas installé Make, vous pouvez directement effectuer les commandes correspondantes en vous référent au Makefile.

```bash
:~/projet_data$ make build
```
<br>
Une fois l'image Docker créée, il nous vous reste plus qu'a effectuer un make run pour lancer le conteneur.

```bash
:~/projet_data$ make run
```
<br>
Et voila, vous pouvez desormais accéder à l'application sur cette adresse : http://localhost:80
<br>

### Serveur

Pour installer le projet sur un serveur, il suffit d'effectuer les même étapes que pour l'installation en local et ouvrir le port 80 de votre serveur, afin de pouvoir accéder à l'application à distance.

## Traitement / Manipulation des données

Le traitement et la manipulation des données ont été effectués vie des [Notebook Jupyter](https://jupyter.org/) <br>
Les Notebook sont stockées dans le dossier notebook situé à la racine du projet. Chaque Notebook traite un dataset en particulier à l'exception de ceux effectués pour rassembler les données de plusieurs dataset.<br>
Les Dataset sont stockés dans le dossier dataset situé à la racine du projet. Celui ci comprend les dataset sources ainsi que ceux ayant été traités, dans deux dossier distincts.<br>
<br>
Les Dataset on étés traités de cette manière :
- Pré-traitement
- Encoding
- Analyse
- Visualisation

## Inteface

L'interface contenant la data story a été créé avec [Streamlit](https://streamlit.io/). <br>
Afin de visualiser nos données, nous avons utilisés une librairie python nommée [Plotly](https://plotly.com/python/) nous permettant de créer tout type de graphs.

## Virtualisation

Afin de deployer l'application facilement et d'éviter tout type de problème liés au librairie utilisées, nous avons conteneuriser l'application avec [Docker](https://www.docker.com/). <br>
<br>
La conteneurisation de l'application s'effectue en suivant plusieurs étape :
- Creation d'un environnement virtuel python
- Exposer le port 80
- Installer les dépendences
- Lancer l'application Streamlit

## Hebergement

L'application est hébergée sur une machine vituelle stockée sur le [cloud d'Oracle](https://www.oracle.com/fr/cloud/). <br>
<br>
Machine Virtuelle spec :
- Os : Ubuntu 20.04
- OCPU : 1
- Memory (GB) : 4
- Public IP : 129.151.227.16

## Souce des données

- [Country area](https://www.kaggle.com/fernandol/countries-of-the-world)
- [Country Code](https://datahub.io/core/country-codes)
- [Country by continent](https://www.kaggle.com/andradaolteanu/country-mapping-iso-continent-region)
- [Country Life Expectancy](https://data.worldbank.org/indicator/SP.DYN.LE00.IN?end=2020&start=1960)
- [Country variable](https://www.kaggle.com/sudalairajkumar/undata-country-profiles)
- [Population growth](https://www.kaggle.com/kuntalmaity/world-population-data)
- [Population growth ONU](https://population.un.org/wpp/Download/Standard/Population/)
