import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import seaborn as sns
import matplotlib.pyplot as plt

df_population = pd.read_csv(r'dataset/cleaned_dataframe/Population_Growth_ONU.csv', index_col=0)
df_density = pd.read_csv(r'dataset/cleaned_dataframe/Population_Density.csv', index_col=0)
df_HDI = pd.read_csv(r'dataset/cleaned_dataframe/Population_HDI.csv', index_col=0)
df_info = pd.read_csv(r'dataset/cleaned_dataframe/Df_Final.csv', index_col=0)
df_life_expectancy = pd.read_csv(r'dataset/cleaned_dataframe/Countries_Life_Expectancy.csv', index_col=0)
df_population_HDI = pd.read_csv(r'dataset/cleaned_dataframe/Population_HDI.csv', index_col=0)
df_surface_area_region = pd.read_csv(r'dataset/cleaned_dataframe/Region_Surface_Area.csv', index_col=0)
df_density_region = pd.read_csv(r'dataset/cleaned_dataframe/Region_Density.csv', index_col=0)
df_world_life_expectancy = pd.read_csv(r'dataset/cleaned_dataframe/World_Life_Expectancy.csv', index_col=0)
df_world_population = pd.read_csv(r"dataset/cleaned_dataframe/World_Population.csv", index_col=0)
df_world_population_prediction = pd.read_csv(r"dataset/cleaned_dataframe/World_Population_Prediction.csv", index_col=0)
df_world_population_formula = pd.read_csv(r"dataset/cleaned_dataframe/World_Population_Formula.csv", index_col=0)
df_world_growth_rate = pd.read_csv(r"dataset/cleaned_dataframe/World_Growth_Rate.csv", index_col=0)
# Dataframe des Regions
df_population_region = df_population
df_population_region["Region"] = df_population_region["Region"].str.split().str[-1]
df_population_region = df_population_region.groupby(['Region']).sum()


def graph_population_top10_country():
    df_graph = df_population.sort_values(by=['2020'], ascending=False).head(10)

    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=df_graph["Country"],
        y=df_graph["1950"],
        name='1950',
        marker_color='lightsalmon'
    ))
    fig.add_trace(go.Bar(
        x=df_graph["Country"],
        y=df_graph["2020"],
        name='2020',
        marker_color='indianred'
    ))

    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45, title="")

    return fig


def graph_density_top10_country():
    df_graph = df_density.sort_values(by=['2020'], ascending=False).head(10)

    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=df_graph["Country"],
        y=df_graph["1950"],
        name='1950',
        marker_color='mediumaquamarine'
    ))
    fig.add_trace(go.Bar(
        x=df_graph["Country"],
        y=df_graph["2020"],
        name='2020',
        marker_color='LightSeaGreen'
    ))

    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45, title="")
    return fig


def graph_population_region():
    df_graph = df_population_region.sort_values(by=['2020'], ascending=False).head(4)

    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=df_graph.index,
        y=df_graph["2020"],
        name='2020',
        marker_color='indianred'
    ))
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45,
                      title="")
    return fig


def df_density_top10_country():
    df = df_info.loc[
        df_info["alpha-3"].isin(df_density.sort_values(by=['2020'], ascending=False).head(10)["ISO3166-1-Alpha-3"])][
        ["country", "Surface area (km2)", "Population in thousands (2017)"]].set_index("country").sort_values(
        by=['Surface area (km2)'], ascending=False)
    return df


def graph_region_surface_area():
    # La russie est full compté en europe
    df_graph = df_surface_area_region.loc[
        df_surface_area_region.index.isin(["Asia", "Africa", "America", "Europe"])].sort_values(
        by=['Surface area (km2)'], ascending=False).head(10)

    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=df_graph.index,
        y=df_graph["Surface area (km2)"],
        name='Surface area (km2)',
        marker_color='palevioletred'
    ))
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45, title="")
    return fig


def graph_region_density():
    df_graph = df_density_region.loc[df_density_region.index].sort_values(by=['2020'], ascending=False).head(10)

    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=df_graph["key_0"],
        y=df_graph["2020"],
        name='2020',
        marker_color='LightSeaGreen'
    ))
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45,
                      title="")
    return fig


def graph_life_expectancy():
    df_world_life_expectancy.index.names = ['Years']
    fig = px.line(df_world_life_expectancy, x=df_world_life_expectancy.index, y="Life Expectancy")
    return fig


def graph_population():
    df_world_population.index.names = ['Years']
    fig = px.line(df_world_population, x=df_world_population.index, y="Population")
    return fig


def graph_population_prediction():
    df_world_population_prediction.index.names = ['Years']
    fig = px.line(df_world_population_prediction, x=df_world_population_prediction.index, y="Population")
    return fig


def graph_population_prediction_formula():
    df_world_population_formula.index.names = ['Years']
    fig = px.line(df_world_population_formula, x=df_world_population_formula.index, y="Population")
    return fig


def graph_world_growth_rate():
    df_world_growth_rate.index.names = ['Years']
    fig = px.line(df_world_growth_rate, x=df_world_growth_rate.index, y="Growth Rate")
    return fig

