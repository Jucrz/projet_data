"""
synopsis: This file contains functions that create maps needed
"""

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

df_life_expectancy = pd.read_csv(r'dataset/cleaned_dataframe/Countries_Life_Expectancy.csv', index_col=0)
df_density = pd.read_csv(r'dataset/cleaned_dataframe/Population_Density.csv', index_col=0)
df_population_growth_onu = pd.read_csv(
    r"dataset/cleaned_dataframe/Population_Growth_ONU.csv"
)

df_population_growth_prediction = pd.read_csv(
    r"dataset/cleaned_dataframe/Population_Growth_Prediction.csv"
)
df_world_population_prediction_formula = pd.read_csv(r"dataset/cleaned_dataframe/World_Population_Prediction_Formula.csv")

def map_population_growth(year: int) -> px.choropleth:
    """
    synopsis: This function create a map of the world population,
    based on the year given as parameter.

    return : The map as a plotly figure.
    """

    df_graph = str(round(df_population_growth_onu[str(year)].sum()))
    numb = " ".join([df_graph[::-1][i:i + 3] for i in range(0, len(df_graph), 3)])[::-1]
    # Create map
    fig = px.choropleth(
        color=1000 * df_population_growth_onu[str(year)],
        hover_name=df_population_growth_onu["Country"],
        locations=df_population_growth_onu["ISO3166-1-Alpha-3"],
        color_continuous_scale=px.colors.sequential.OrRd,
        range_color=(0, 500000000),
        labels={"color": "Population", "locations": "Country Code"},
    )

    # Add annotation to the map
    layout = go.Layout(
        title=go.layout.Title(text=f"Population en {year} : {numb} de personnes", x=0.5),
        font=dict(size=14),
        width=750,
        height=350,
        margin=dict(l=0, r=0, b=0, t=30),
    )

    fig.update_layout(layout)

    return fig


def map_population_growth_prediction(year: int) -> px.choropleth:
    """
    synopsis: This function create a map of the world population prediction,
    based on the year given as parameter.

    return : The map as a plotly figure.
    """
    df_graph = str(round(df_population_growth_prediction[f"{year}-01-01 00:00:00"].sum()))
    numb = " ".join([df_graph[::-1][i:i + 3] for i in range(0, len(df_graph), 3)])[::-1]
    # Create map
    fig = px.choropleth(
        color=df_population_growth_prediction[f"{year}-01-01 00:00:00"],
        hover_name=df_population_growth_prediction["Country"],
        locations=df_population_growth_prediction["ISO3166-1-Alpha-3"],
        color_continuous_scale=px.colors.sequential.OrRd,
        range_color=(0, 500000000),
        labels={"color": "Population", "locations": "Country Code"},
    )

    # Add annotation to the map
    layout = go.Layout(
        title=go.layout.Title(text=f"Population en {year} : {numb} de personnes", x=0.5),
        font=dict(size=14),
        width=750,
        height=350,
        margin=dict(l=0, r=0, b=0, t=30),
    )

    fig.update_layout(layout)

    return fig


def map_population_density(year: int):
    fig = px.choropleth(df_density, locations="ISO3166-1-Alpha-3",
                        color=df_density[str(year)],
                        hover_name="Country",
                        color_continuous_scale=px.colors.sequential.Tealgrn,
                        range_color=(0, 200))

    layout = go.Layout(
        title=go.layout.Title(
            text="",
            x=0.5
        ),
        font=dict(size=14),
        width=750,
        height=350,
        margin=dict(l=0, r=0, b=0, t=30)
    )

    fig.update_layout(layout)

    return fig


def map_life_expectancy(year: int):
    fig = px.choropleth(df_life_expectancy, locations="Country Code",
                        color=df_life_expectancy[str(year)],
                        hover_name=df_life_expectancy.index,
                        color_continuous_scale=px.colors.sequential.Sunsetdark,
                        range_color=(60, 90))

    layout = go.Layout(
        title=go.layout.Title(
            text="Worldwide population",
            x=0.5
        ),
        font=dict(size=14),
        width=750,
        height=350,
        margin=dict(l=0, r=0, b=0, t=30)
    )

    fig.update_layout(layout)

    return fig


def map_population_growth_prediction_formula(year: int) -> px.choropleth:
    df_graph = str(round(df_world_population_prediction_formula[str(year)].sum()))
    numb = " ".join([df_graph[::-1][i:i + 3] for i in range(0, len(df_graph), 3)])[::-1]

    fig = px.choropleth(df_world_population_prediction_formula, locations="ISO3166-1-Alpha-3",
                        color=df_world_population_prediction_formula[str(year)],
                        hover_name="Country",
                        color_continuous_scale=px.colors.sequential.OrRd,
                        range_color=(0, 500000))

    layout = go.Layout(
        title=go.layout.Title(
            text=f"Population en {year} : {numb} de personnes", x=0.5
        ),
        font=dict(size=14),
        width=750,
        height=350,
        margin=dict(l=0, r=0, b=0, t=30)
    )

    fig.update_layout(layout)

    return fig



