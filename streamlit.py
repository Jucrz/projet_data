"""
:synopsis: Web interface where the data story is displayed
"""

import streamlit as st  # pylint: disable=(import-self)
from src.map import map_population_growth, map_population_growth_prediction, map_population_growth_prediction_formula, map_population_density, map_life_expectancy
from src.graph import graph_population_top10_country
from src.graph import graph_density_top10_country
from src.graph import graph_population_region
from src.graph import df_density_top10_country
from src.graph import graph_region_surface_area
from src.graph import graph_region_density
from src.graph import graph_life_expectancy
from src.graph import graph_population
from src.graph import graph_population_prediction, graph_population_prediction_formula, graph_world_growth_rate
import streamlit.components.v1 as components

# Set up page
st.set_page_config(  # pylint: disable=(no-member)
    page_title="Population evolution data story",
    page_icon=r"./img/world_population.png",
    layout="wide",
    initial_sidebar_state="collapsed",
    menu_items={
        "About": "# This main to describe the population evolution all over the world"
    },
)

# Title of the data story
st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000099;font-size:80px;text-align: center; margin-bottom: 5%;">{"La croissance démographique mondiale"}</h1>', unsafe_allow_html=True)
components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:80%;" /> """)
st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:48px;text-align: center; margin-bottom: 4%;">{"Répartition de la population à travers le monde"}</h1>', unsafe_allow_html=True)

col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    # Introduction
    st.markdown(f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:24px;margin-bottom:10%;">{"Cela fait des décennies que la population ne fait que de croitre à un rythme effréné, depuis 1950 elle a plus que triplé 2,5millards contre quasiment 8milliards aujourd’hui. Cela résulte d’un développement de la technologie et de la médecine qui a nettement amélioré la qualité de vie et donc par défaut engendré une forte croissance démographique. Même si cet accroissement de la population résulte de progrès qui nous sont bénéfique, ce qu’il peut engendrer ne l’est pas tout autant."}</h1>', unsafe_allow_html=True)


col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center; margin-bottom: 9%;">{"Evolution de la population du monde"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_population(), use_container_width=True)

with col3:
    # Population growth map
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:28px;text-align: center;">{"Population totale par pays"}</h1>',
        unsafe_allow_html=True)
    # Slider
    year_growth_map = st.slider("Year", 1950, 2020, 1950)  # pylint: disable=(no-member)
    # Interactive graph
    st.plotly_chart(  # pylint: disable=(no-member)
        map_population_growth(year_growth_map),
        use_container_width=True,
    )
    # Data source for the graph
    #st.write(  # pylint: disable=(no-member)
    #    "Source: [United Nation Department of Economic and Social Affairs]"
    #    "(https://population.un.org/wpp/Download/Standard/Population/)"
    #)

components.html("""<hr style="font-family: Garamond, serif;height:2px;border:none;color:#333;background-color:#333;width:20%; margin-top:4%;" /> """)

col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:28px;text-align: center;">{"Populations des 10 pays les plus peuplés 1950/2020"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_population_top10_country(), use_container_width=True)
with col3:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:white;font-size:28px;text-align: center;">{"0"}</h1>',
        unsafe_allow_html=True)
    st.markdown(
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:24px;width:85%;margin-top:10%;margin-left:50px;">{"Comme dit précédemment la population à plus que triplé depuis 1950 et nous l observons très bien ici. Nous pouvons aussi constater que la répartition de la population est assez inégale car même au sein des 10 pays les plus habité on voit une nette différence entre l’Inde et la Chine contre le reste. Ils représentent à eux deux plus de un tier de la population mondiale et une majeure partie des habitants de l Asie."}</h1>',
        unsafe_allow_html=True)

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:20%; margin-top:4%;" /> """)

col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:white;font-size:28px;text-align: center;">{"0"}</h1>',
        unsafe_allow_html=True)
    st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:24px;width:85%;margin-top:10%;margin-right:200px;">{"En effet l Asie représente aujourd hui plus de la moitié de la population totale mondiale, 4,5 milliards d habitants à elle seule."}</h1>', unsafe_allow_html=True)
with col3:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:28px;text-align: center;">{"Population de l Asie, Afrique, Amérique et de l Europe en 2020"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_population_region(), use_container_width=True)

                                                                            ###################
                                                                            ##### DENSITE #####
                                                                            ###################

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:50%; margin-top:5%;" /> """)

st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:48px;text-align: center; margin-top:5%; margin-bottom: 8%;">{"Pays les plus denses"}</h1>', unsafe_allow_html=True)

col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:white;font-size:28px;text-align: center;">{"0"}</h1>',
        unsafe_allow_html=True)
    st.markdown(
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;">{"Nous pouvons remarquer plusieurs points de densité apparaitre sur la carte mais ils sont le plus concentré vers l Asie et l Europe mais les pays les plus denses ne sont pas forcement ceux qui ce démarquent sur cette carte."}</h1>',
        unsafe_allow_html=True)

with col3:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center;">{"Densité par pays"}</h1>',
        unsafe_allow_html=True)
    # Slider
    density_map = st.slider("Year", 1950, 2020, 2020)  # pylint: disable=(no-member)
    # Interactive graph
    st.plotly_chart(  # pylint: disable=(no-member)
        map_population_density(density_map),
        use_container_width=True,
    )

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:20%; margin-top:4%;" /> """)

col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:28px;text-align: center; margin-bottom: -5%;">{"Densité des 10 pays les plus dense 1950/2020s"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_density_top10_country(), use_container_width=True)
with col3:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:28px;text-align: center; margin-bottom: 10%;">{"Superficie et population des 10 pays les plus dense "}</h1>',
        unsafe_allow_html=True)
    st.dataframe(df_density_top10_country())

col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    st.markdown(f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;">{"La densité se calcule par rapport au nombre d habitant divisé par la superficie de la région observé. Ici la densité est très élevé à Monaco et Macau car la surface des deux pays est plutot petite tandis que leurs populations n est pas si élevé que ça, 39 000 habitant pour Monaco et seulement 2km² de superficie. Le Bangladesh quant à lui entre dans ce top 10 avec 147 000km² et avec plus de 164millions d habitants (8ème pays le plus peuplé) il est le seul pays à être dans les pays les plus peuplés et les plus denses à la fois."}</h1>', unsafe_allow_html=True)

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:20%; margin-top:4%;" /> """)

col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:28px;text-align: center;">{"Superficie de l Asie, Amérique, Afrique et de l Europe"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_region_surface_area(), use_container_width=True)
with col3:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:28px;text-align: center;">{"Densité de l Asie, Europe, Afrique et de l Amérique "}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_region_density(), use_container_width=True)

col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    st.markdown(f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;">{"Même si l Asie et l Amérique sont au coude à coude niveau superficie, environs 45 millions de km² chacun,  l Asie reste près de 5 fois plus dense que l Amerique. Néanmoins l écart de densité entre les autres régions du monde est bien moins flagrante comparé à l écart de population."}</h1>', unsafe_allow_html=True)

                                                                            ####################
                                                                            ##### Life Exp #####
                                                                            ####################


components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:50%; margin-top:5%;" /> """)

st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:48px;text-align: center; margin-top:5%; margin-bottom: 4%;">{"Evolution de l esperance de vie"}</h1>', unsafe_allow_html=True)


col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    # Introduction
    st.markdown(f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:24px;margin-bottom:10%;">{"L esperance de vie a nettement augmenté depuis 1960, près de 20 ans d age moyen en plus aujourd hui. On peut remarquer que la courbe de la population et de l esperance de vie sont plutot similiaires et donc en conclure que cette dernière a surement eu un impact sur la croissance démographique même si bien sur ce n est pas le seul facteur."}</h1>', unsafe_allow_html=True)


col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center; margin-bottom: 9%;">{"Evolution de l esperance de vie dans le monde"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_life_expectancy(), use_container_width=True)
with col3:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center;">{"Esperance de vie par pays"}</h1>',
        unsafe_allow_html=True)
    # Slider
    life_exp_map = st.slider("Year", 1960, 2020, 1960)  # pylint: disable=(no-member)
    # Interactive graph
    st.plotly_chart(  # pylint: disable=(no-member)
        map_life_expectancy(life_exp_map),
        use_container_width=True,
    )

                                                                            ################################
                                                                            ##### Taux d'accroissement #####
                                                                            ################################




components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:50%; margin-top:5%;" /> """)

st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:48px;text-align: center; margin-top:5%; margin-bottom: 4%;">{"Evolution du taux d accroissement"}</h1>', unsafe_allow_html=True)

col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:white;font-size:28px;text-align: center;margin-bottom: 9%;">{"0"}</h1>',
        unsafe_allow_html=True)
    st.markdown(
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;">{"Le taux d accroissement a connu une hausse de 1950 pour atteindre son pique en 1960 période du baby boom mais depuis les années 90 il ne fait que de chuter."}</h1>',
        unsafe_allow_html=True)
with col3:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center; margin-bottom: 9%;">{"Evolution de l esperance de vie dans le monde"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_world_growth_rate(), use_container_width=True)



                                                                                #######################
                                                                                ##### Prédictions #####
                                                                                #######################

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:50%; margin-top:5%;" /> """)

st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:48px;text-align: center; margin-top:5%;">{"Prédiction de la population"}</h1>', unsafe_allow_html=True)

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:20%; margin-top:4%;" /> """)

st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:40px;text-align: center; margin-bottom: 4%;">{"En utilisant de l autorégréssion"}</h1>', unsafe_allow_html=True)


col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    st.markdown(f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%; margin-bottom: 9%;">{"Les modèles autorégressifs sont des modèles pouvant être appliqués sur des séries chronologiques. Ils sont utilisés pour prédire les données futures en fonction des observations précédentes. Les prédictions ne sont donc pas vraiment précises car elles ne prennent pas en compte différentes variables qui sont impactantes dans l évolution de la population."}</h1>', unsafe_allow_html=True)



col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center; margin-bottom: 9%;">{"Prediction de l evolution de la population du monde"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_population_prediction(), use_container_width=True)
with col3:
    # Population growth prediction map
    # Header
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center;">{"Prediction de la population totale par pays"}</h1>',
        unsafe_allow_html=True)
    # Slider
    year_growth_map_prediction = st.slider("Year", 2021, 2050, 2021)  # pylint: disable=(no-member)
    # Interactive graph
    st.plotly_chart(  # pylint: disable=(no-member)
        map_population_growth_prediction(year_growth_map_prediction),
        use_container_width=True,
    )

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:20%; margin-top:4%;" /> """)

st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:40px;text-align: center; margin-bottom: 4%;">{"En utilisant une formule"}</h1>', unsafe_allow_html=True)

col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    # Introduction
    st.markdown(f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:24px;margin-bottom:10%;">{"Pour cette formule nous avons calculé le taux d accroissement de la population par pays de 2019, pour cela nous avons divisé la population de 2018 par celle de 2019. En suite nous avons calculé la population de l année suivante en multipliant celle de l année actuelle par ce taux d accroissement. Ici les prédictions sont légèrements plus faibles que celles données précédemment, 500 millions d habitants de moins prédit pour 2050. (Cette prédiction à été réalisé en supposant que le taux d accroissement resterait plus ou moins stable jusqu à 2050, même si ce ne sera très probablement pas le cas.)"}</h1>', unsafe_allow_html=True)


col1, col2, col3, col4 = st.columns([1, 2, 2, 1])
with col2:
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center; margin-bottom: 9%;">{"Prediction de l evolution de la population du monde"}</h1>',
        unsafe_allow_html=True)
    st.plotly_chart(graph_population_prediction_formula(), use_container_width=True)
with col3:
    # Population growth prediction map
    # Header
    st.markdown( # pylint: disable=(no-member)
        f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:30px;text-align: center;">{"Prediction de la population totale par pays"}</h1>',
        unsafe_allow_html=True)
    # Slider
    year_growth_map_prediction_formula = st.slider("Year", 1950, 2050, 1950)  # pylint: disable=(no-member)
    # Interactive graph
    st.plotly_chart(  # pylint: disable=(no-member)
        map_population_growth_prediction_formula(year_growth_map_prediction_formula),
        use_container_width=True,
    )

components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:20%; margin-top:4%;" /> """)


col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    st.markdown(f'<h1 style="text-align:center;font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;">{"Nous sommes conscient que les prédictions effectuées ne sont pas précises car ils nous faudrait beaucoup plus de variables pour augmenter notre précision. Mais nous tenions tout de même à en réaliser une pour avoir une idée même plus ou moins biaisée de comment pourrait évoluer notre population d ici 2050."}</h1>', unsafe_allow_html=True)


                                                                                ######################
                                                                                ##### Conclusion #####
                                                                                ######################


components.html("""<hr style="height:2px;border:none;color:#333;background-color:#333;width:50%; margin-top:5%;" /> """)

st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000066;font-size:48px;text-align: center; margin-top:4%;">{"Conclusion"}</h1>', unsafe_allow_html=True)



col1, col2, col3 = st.columns([1, 2, 1])
with col2:
    st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;text-align:center;">{"Comme dit en introduction nous avons pu constater au cours de notre analyse que la population a connu une forte croissance même si cette dernière est répartie plus ou moins inégalement. Nos prédictions montrent une progression exponentielle de la population, cependant nous avons aussi vu que le taux d accroissement lui ne faisait que de diminuer depuis les années 90. On peut donc en conclure que malgrés nos prédictions la population va tendre à ce stabiliser aux cours du temps."}</h1>', unsafe_allow_html=True)



components.html("""<hr style="margin-top:4%;height:2px;border:none;color:#333;background-color:#333;width:80%;" /> """)


col1, col2, col3 = st.columns([1, 2, 1])
with col1:
    st.write(  # pylint: disable=(no-member)
        "Country area : [Kaggle]"
        "(https://www.kaggle.com/fernandol/countries-of-the-world)")
    st.write(  # pylint: disable=(no-member)
        "Country code : [GitHub]"
        "(https://datahub.io/core/country-codes)")
    st.write(  # pylint: disable=(no-member)
        "Country by continent : [Kaggle]"
        "(https://www.kaggle.com/andradaolteanu/country-mapping-iso-continent-region)")
    st.write(  # pylint: disable=(no-member)
        "Country Life Expectancy : [The World Bank]"
        "(https://data.worldbank.org/indicator/SP.DYN.LE00.IN?end=2020&start=1960)")
    st.write(  # pylint: disable=(no-member)
        "Country variable : [Kaggle]"
        "(https://www.kaggle.com/sudalairajkumar/undata-country-profiles)")
    st.write(  # pylint: disable=(no-member)
        "Population growth : [Kaggle]"
        "(https://www.kaggle.com/kuntalmaity/world-population-data)")
    st.write(  # pylint: disable=(no-member)
        "Population growth ONU : [United Nations]"
        "(https://population.un.org/wpp/Download/Standard/Population)")

with col3:
    st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;text-align:center;">{"Datastory rédigée par : "}</h1>', unsafe_allow_html=True)
    st.markdown(f'<h1 style="font-family: Garamond, serif;color:#000033;font-size:24px;margin-top:5%;text-align:center;">{"CRUZ Julien, LARRODE Alexis "}</h1>', unsafe_allow_html=True)


