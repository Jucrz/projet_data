.PHONY: help

.DEFAULT_GOAL := build

### DOCKER TASKS ###

build: # Build the container
	docker build -t data-project .

build-nc: # Build the container without caching
	docker build --no-cache -t data-project .

run: # Run container on port configured in `config.env`
	docker run -i -t -p 80:80 -d data-project

stop: # Stop and remove a running container
	docker stop data-project; docker rm data-project

